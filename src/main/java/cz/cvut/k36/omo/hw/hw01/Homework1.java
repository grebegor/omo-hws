package cz.cvut.k36.omo.hw.hw01;

public class Homework1 {
    // Static variable to count calls to the static method i() across all instances
    private static int globStaticCount = 0;

    // Instance variable to count calls to the method h() for this particular instance
    private int instCount = 0;

    // Method that always returns true
    public boolean f() {
        return true;
    }

    // Static method that always returns false
    public static boolean g() {
        return false;
    }

    // Method that returns the number of times it has been called on this instance
    public int h() {
        return ++instCount;
    }

    // Method that returns the total number of times i() has been called on any instance
    public int i() {
        return ++globStaticCount;
    }

    // Main method for simple testing (optional)
    public static void main(String[] args) {
        Homework1 hw1 = new Homework1();
        Homework1 hw2 = new Homework1();

        System.out.println("hw1.f(): " + hw1.f());
        System.out.println("Homework1.g(): " + Homework1.g());
        System.out.println("hw1.h(): " + hw1.h());
        System.out.println("hw1.h(): " + hw1.h());
        System.out.println("hw2.h(): " + hw2.h());
        System.out.println("hw1.i(): " + hw1.i());
        System.out.println("hw2.i(): " + hw2.i());
        System.out.println("hw1.i(): " + hw1.i());
    }
}
